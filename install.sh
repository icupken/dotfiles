#!/bin/bash
echo "Install yay..."
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si --noconfirm
cd ..
echo "Yay installed."

# ------xfwm4 install----------
sudo pacman -S alsa-utils fd nitrogen alacritty nvidia f2fs-tools ntfs-3g pcmanfm-qt gvfs \
firefox tint2 gxkb helix xorg xorg-xinit pulseaudio ttf-jetbrains-mono sxhkd hunspell-ru \
pavucontrol ttf-iosevka-nerd exa xfwm4 picom dunst okular  --noconfirm

yay -S polybar ttf-material-design-icons-extended nerd-fonts-jetbrains-mono betterlockscreen ulauncher --noconfirm

cp -r .config ~/
mkdir -p ~/.local/share/
cp -r fonts ~/.local/share/
cp .xinitrc ~/
cp .bashrc ~/
echo 'set completion-ignore-case on' >> ~/.inputrc
sudo systemctl --user enable --now ulauncher
localectl --no-convert set-x11-keymap us,ru "" "" grp:alt_shift_toggle
echo "Instalation end."
