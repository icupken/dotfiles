# If not running interactively, don't do anything
[[ $- != *i* ]] && return

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec startx
fi


alias exa='exa -l'
alias update='sudo pacman -Syu --noconfirm'
alias install='sudo pacman -Sy'


#make st nice
alias load="kill -USR1 $(pidof st)"
alias  use="xrdb merge"

PS1='[\u@\h \W]\$ '
export PATH="/usr/bin/rust-analyzer:$PATH"
export PATH="~/.local/bin:$PATH"
source "$HOME/.cargo/env"
